<?php

namespace Example\Models;

use Nakashima\Mongo\Model as Model;

/**
 * Class Link
 * @package Example\Models
 */
class Link extends Model
{
    /** @var string $collection */
    protected $collection = 'Link';
    /** @var string $primaryKey */
    protected $primaryKey = '_id';
}
