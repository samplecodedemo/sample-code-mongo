<?php

namespace Example\Models;

use Nakashima\Mongo\Model as Model;

/**
 * Class Client
 * @package Example\Models
 */
class Client extends Model
{
    /** @var string $collection */
    protected $collection = 'Client';
    /** @var string  $primaryKey*/
    protected $primaryKey = '_id';

    /**
     * @param array $options
     * @return bool
     */
    public function save()
    {
        $this->attributes['db'] = str_replace(".","-",$this->domain);
        return parent::save();
    }
}
