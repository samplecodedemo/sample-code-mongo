<?php

namespace Example\Models;

use Nakashima\Mongo\Model as Model;

/**
 * Class Product
 * @package Example\Models
 */
class Product extends Model
{
    /** @var string $collection */
    protected $collection = 'Product';
    /** @var string $primaryKey */
    protected $primaryKey = '_id';
}
