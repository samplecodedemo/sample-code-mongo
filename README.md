Sample custom php Mongo Models

I was needing of a simple way to deal with several mongo databases.
Basically I had one administrative database to save all my clients domains.
And several others databases, one per client to save their product links.

Example:

//Creating a new Client at database "master"

$client = new Client();

$client->domain = 'www.teste.com';

$client->xml = $xml;

$client->save();

//Creating a new Link at $client->db

$link = new Link($client->db);

$link->url = $url;

$link->save();

//Searching

$link->find($link->_id);

$link->find(array('url' => "www.linkteste.com"));

$product->find(['$text' => ['$search' => $words]]);