<?php

use Test\Factory\EntityFactory;
use Nakashima\Mongo\ConnectionHandler;

abstract class Base extends \PHPUnit_Framework_TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /** @var  EntityFactory $entityFactory */
    protected $entityFactory;

    /**
     * Instanciates EntityFactory.
     *
     */
    public function __construct()
    {
        $this->setEntityFactory(new EntityFactory());
        ConnectionHandler::setDefaultDatabase('master-test');
    }
    /**
     * @return EntityFactory $entityFactory
     */
    public function getEntityFactory()
    {
        return $this->entityFactory;
    }

    public function setEntityFactory($entityFactory)
    {
        $this->entityFactory = $entityFactory;
    }
}
